const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");

//Checkout the items in the cart of the user
router.post("/", auth.verifyUser, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	orderController.checkOut(data).then(resultFromController => res.send(resultFromController)
	)
});

//Retrieve all orders by Admin only
router.get("/all", auth.verifyAdmin, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController)
	)
});

// Get order of the logged in user
router.get("/myOrders", auth.verifyUser, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	orderController.myOrder(data).then(resultFromController => res.send(resultFromController)
	)
})

// Get total earnings (admin)
router.get("/totalSales", auth.verifyAdmin, (req, res) => {
	const data = auth.decode(req.headers.authorization)
	orderController.totalSales(data).then(resultFromController => res.send(resultFromController)
	)
});

module.exports = router;