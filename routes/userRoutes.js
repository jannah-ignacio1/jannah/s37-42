const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Check if email already exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
});

//User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//User Login/Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Get profile of all users
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getUser({userId:userData.id}).then(resultFromController => res.send(resultFromController))
});

//Get one user's profile
router.get("/:userId", auth.verify, (req, res) => {
	userController.getOneUser(req.params.userId).then(resultFromController => res.send(resultFromController))
});

//Updating of user's profile
router.put("/:userId/update", auth.verify, (req, res) => {
	const userData = {
		userId: req.params.userId,
		updateUser: req.body
	}
	userController.userUpdate(userData).then(resultFromController => res.send(resultFromController))
});

//Extra: Enrollment of user as admin
router.put("/:userId/setAdmin", auth.verifyAdmin, (req, res) => {
	const dataAdmin = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: req.params.userId,
		updateUser: req.body
	}
	console.log(dataAdmin)
	userController.adminEnrollment(dataAdmin).then(resultFromController => res.send(resultFromController))
});

module.exports = router;