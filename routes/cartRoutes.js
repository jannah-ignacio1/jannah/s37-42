const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartControllers");
const auth = require("../auth");

//Get the cart of user
router.get("/", auth.verifyUser, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	cartController.getCart(data).then(resultFromController => res.send(resultFromController)
	)
});

//Create cart (req.body -> productId and quantity)
router.post("/", auth.verifyUser, (req, res) => {
	const data = {
		user: auth.decode(req.headers.authorization),
		reqBody: req.body
		};
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController)
	)
});

//Remove item from cart
router.delete("/:productId", auth.verifyUser, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	};
	cartController.removeProduct(data).then(resultFromController => res.send(resultFromController)
	)
});

//delete cart
router.delete("/delete", auth.verifyUser, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	cartController.deleteCart(userId).then(resultFromController => res.send(resultFromController)
	)
});

module.exports = router;

