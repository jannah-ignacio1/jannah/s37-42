const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");


//Add product by Admin only
router.post("/", auth.verifyAdmin, (req,res) => {
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
});

//Retrieve all active products by anyone
router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

//Retrieve single active product by anyone
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//Retrieve all products by Admin only
router.post("/all", auth.verifyAdmin, (req,res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

//Update product by Admin only
router.put("/:productId", auth.verifyAdmin, (req,res) => {
	const productData = {
		productId:req.params.productId,
		updateProduct: req.body
	}
	productController.updateProduct(productData).then(resultFromController => res.send(resultFromController));
});

//Archive product by Admin only
router.put("/:productId/archive", auth.verifyAdmin, (req,res) => {
	const archiveData = {
		productId: req.params.productId
	}
	//console.log(archiveData)
	productController.archiveProduct(archiveData).then(resultFromController => res.send(resultFromController));
});

//unarchive a product by Admin only
router.put("/:productId/unarchive", auth.verifyAdmin, (req, res) => {
	const unarchiveData = {
		productId: req.params.productId
	}
	productController.unarchiveProduct(unarchiveData).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
