const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: `User`,
    },
    products: [{
        productId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: `Product`
        },
        description: {
            type: String
        },
        category: {
            type: String
        },
        quantity: {
            type: Number,
            default: 1,
            min: 1
        },
        price: {
            type: Number,
            required: true
        }        
    }],
    bill:{
        type: Number,
        required: true
    },
    purchasedOn:{
        type: Date,
        default: new Date()
    }
}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);
