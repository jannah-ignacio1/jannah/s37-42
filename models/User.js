const mongoose = require("mongoose");
const cartSchema = require("./Cart");

	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "First Name is required."]
		},
		lastName: {
			type: String,
			requried: [true, "Last Name is required."]
		},
		email: {
			type: String,
			required: [true, "Email Address is required."],
			unique: true
		},
		password: {
			type: String,
			required: [true, "Password is required."],
		},
		mobileNo: {
			type: String,
			required: [true, "Mobile Number is required."]
		},
		address: [{
			street: {
				type: String,
				required: [ true, "Street is required."]
			},
			barangay: {
				type: String,
				required: [true, "Barangay is required."]
			},
			city: {
				type: String,
				required: [true, "City is required."]
			}
		}],
		isAdmin: {
			type: Boolean,
			default: false
		},
		/*cart:[{
	        cartId:{
	            type: mongoose.Schema.Types.ObjectId,
	            ref: `Cart`
	        },
	        bill:{
	            type: Number,
	            default: 1,
	        }
       }]*/
	},
		{timestamps: true}
	);

module.exports = mongoose.model("User", userSchema);