const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	code: {
		type: String,
		required: [true, "Code is required."]
	},
	name: {
		type: String,
		required: [true, "Product Name is required."]
	},
	description: {
		type: String,			
		requried: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	category: { //thick crust or thin crust
		type: String,
		required: [true, "Category is required."]
	},
	stocks: {
		type: Number,
		default: 1
	},
	isActive: {
		type: Boolean,
		default: true
	},
	url: {
		type: String,
		required: [true, "Url is required."]
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
}
);

module.exports = mongoose.model("Product", productSchema);