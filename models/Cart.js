const mongoose = require("mongoose");
const orderSchema = require(`./Order`);
const userSchema = require(`./User`);

const CartSchema = new mongoose.Schema({

    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: `User`,
        required: true
    },
    products: [{
        productId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: `Product`,
            required: [true, "Product is required."]
        },
        category: {
            type: String
        },
        quantity: {
            type: Number,
            default: 1,
            min: 1
        },
        price: {
            type: Number,
            required: true
        }
    }],
    bill:{
        type: Number,
        required: true
    },
    modifiedOn: {
        type: Date,
        default: new Date()
  }
})

module.exports = mongoose.model("Cart", CartSchema);
