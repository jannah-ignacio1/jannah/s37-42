const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4001;
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const cartRoutes = require("./routes/cartRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();

mongoose.connect("mongodb+srv://admin:123@course.yg49u.mongodb.net/Capstone_02?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/carts", cartRoutes);
app.use("/orders", orderRoutes);

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});