const jwt = require("jsonwebtoken")

const secret = "PizzaDeliveryAPI";

//Token Creation
module.exports.createAccessToken = (user) => {
	
	//PAYLOAD (JWT token)
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//Verify Token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
	token = token.slice(7, token.length);

	return jwt.verify(token, secret, (error, data) => { 
		if (error) {
			return res.send({auth:"Failed"})
		} else {
			next();
		}
	})
}	else {
	return res.send({auth:"Failed"})
}
};

//Decode Token
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null
	}
}

/*Verify Token for Authorization of Non-Admin User OR Admin
module.exports.verifyAndAuthorization = (req, res, next) => {
	jwt.verify(req, res,() => {
		if(req.userId === req.params.id || req.user.isAdmin){
			next();
		} else {
			return res.send("No Access.")
		}
	})
};*/

//Verify Token for User or Admin
module.exports.verifyUser = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
	token = token.slice(7, token.length);

	return jwt.verify(token, secret, (error, data) => { 
		if (error) {
			return res.send({auth:"Failed"})
		} if(data.isAdmin == false) {
			next();
		} else {
			return res.send("Only Non-Admin is allowed.")
		}
	})
}	else {
	return res.send({auth:"Failed"})
}
};

//Verify Token for Admin only
module.exports.verifyAdmin = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
	token = token.slice(7, token.length);

	return jwt.verify(token, secret, (error, data) => { 
		if (error) {
			return res.send({auth:"Failed"})
		} if(data.isAdmin == true) {
			next();
		} else {
			return res.send("No Access.")
		}
	})
}	else {
	return res.send({auth:"Failed"})
}
};
