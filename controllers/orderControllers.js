const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const Cart = require("../models/Cart");
const cartController = require("./cartControllers");

//checkout items in the cart of user
module.exports.checkOut = (data) => {
	return Cart.findOne({userId: data.id}).then(result => {
		if(result) {
			let order = new Order({
				userId: result.userId,
				products: result.products,
				bill: result.bill
			});

			return order.save().then((order, err) => {
				if(err) {
					return false;
				}
				else {
					//console.log(order.userId);
					cartController.deleteCart(order.userId);
					return order;
				}
			})
		}
		else {
			return false;
		}
	})
}

//Get all orders by Admin only
module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
		return result //true
	})
}

//Retrieve authenticated user's orders
module.exports.myOrder = (data) => {
	return Order.find({userId: data.id}).then(result => {
		if(result == null) {
			return false
		} else {
			return result
		}
	})
}

module.exports.totalSales = async (payload) => {
		return Order.aggregate([
			{ $match: { bill: {$gte: 0} } },
    		{ $group: { _id: null, totalSales: { $sum: "$bill" } } }
    		])
		.then(result => {
			return result;
		})
}