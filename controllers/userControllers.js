const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

//Check if email already exists
//will be used for validation during user registration
module.exports.checkEmail = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if(result.length > 0) {
			return true //Email exists.
		} else {
			return false //No registered email yet.
		}
	})
}

//User Registration
module.exports.registerUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then((result) => {
		if (result == null) {
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo,
				address: [{
					street: reqBody.address.street,
					barangay: reqBody.address.barangay,
					city: reqBody.address.city
				}]
			});
			//console.log(newUser)
			return newUser.save().then((savedUser, error) => {
				if (error) {
					//console.log(error)
					return false
				} else {
					return true //"User is successfully registered."
				}
			})
		
		} else {
			return false //"Email already exists."
		}
	})
};

//User Login/Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => { 
		if(result == null){
			return false //"Email is not found."
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false //"Email or Password did not match."
			}
		}
		})
};

//Get profile of all users
module.exports.getUser = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = ""
		return result
	})
}	

//Get one user's profile
module.exports.getOneUser = (reqParams) => {
	return User.findById(reqParams).then(result => {
			return result
	})
}

//Updating of user's profile
module.exports.userUpdate = (userData) => {
	const updatedProfile = ({	
		firstName: userData.updateUser.firstName,
		lastName: userData.updateUser.lastName,
		email: userData.updateUser.email,
		password: bcrypt.hashSync(userData.updateUser.password, 10),
		mobileNo: userData.updateUser.mobileNo,
		address: userData.updateUser.address
		})	
	return User.findByIdAndUpdate(userData.userId, updatedProfile).then((result, error) => {
		if (error) {
			return false //"User data cannot be updated."
		} else {
			return result //`User data is updated successfully.` 
		}
	})
}

//Extra: Enrollment of user as admin
module.exports.adminEnrollment = (dataAdmin) => {
	return User.findById(dataAdmin.userId).then((result, error) => {
			result.isAdmin = dataAdmin.updateUser.isAdmin
		 return result.save().then((saveUser, error) => {
			if (error) {
				return false //"User data cannot be updated."
			} else {
				return true //`User data (isAdmin:${result.isAdmin}) is updated successfully.`
			}
		})	
	})
}