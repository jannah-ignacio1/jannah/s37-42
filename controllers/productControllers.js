const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");


//Add product by Admin only
module.exports.addProduct = (reqBody) => {
	return Product.findOne({code:reqBody.code} || {name:reqBody.name}).then((result) => {
		if(result == null) {
			let newProduct = new Product ({
				code: reqBody.code,
				name: reqBody.name,
				description: reqBody.description,
				category: reqBody.category,
				price: reqBody.price,
				stocks: reqBody.stocks,
				url: reqBody.url
			})
			console.log(newProduct)	

		return newProduct.save().then((product, error) => {
			if(error) {
				return false //"Check your inputs."
			} else {
				return true //"Product is added successfully."
			}
		})
	} else {
		return false //`Product already exists.`
	}
	})
};

//Retrieve all active products by anyone
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive:true}).then((result, error) => {
		return result //true
	})
};

//Retrieve single active product by anyone
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then((result, error) => {
		if(result == undefined) {
			return false //`Product Id ${reqParams.productId} could not be found.`
		} else {
		return result
		}
	})
};

//Retrieve all products by Admin only
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result //true
	})
};

//Update product by Admin only
module.exports.updateProduct = (productData) => {
	return Product.findById(productData.productId).then((result, error) => {
		if(result == null){
			return false //`Product Id ${productData.productId} could not be found.`
		} else {
			let updatedProduct = ({
				code: productData.updateProduct.code,
				name: productData.updateProduct.name,
				description: productData.updateProduct.description,
				price: productData.updateProduct.price,
				category: productData.updateProduct.category,
				url: productData.updateProduct.url,
				quantity: productData.updateProduct.quantity
			})
			return Product.findByIdAndUpdate(productData.productId, updatedProduct).then((product, error) => {
				if(error) {
					return false
				} else {
					return result //"Product is updated successfully."
				}
			})
		}
	})
}

//Archive product by Admin only
module.exports.archiveProduct = (archiveData) => {
	return Product.findById(archiveData.productId).then((result, error) => {
		if(result) {
			result.isActive = false
			return result.save().then((archivedProduct, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
}

//Unarchive product by Admin only
module.exports.unarchiveProduct = (unarchiveData) => {
	return Product.findById(unarchiveData.productId).then((result, error) => {
		if(result) {
			result.isActive = true
			return result.save().then((unarchivedProduct, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		} 
	})
}