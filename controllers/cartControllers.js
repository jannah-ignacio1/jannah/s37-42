const Product = require("../models/Product");
const User = require("../models/User");
const Cart = require("../models/Cart");
const Order = require("../models/Order");

//Get the cart of user
module.exports.getCart = (data) => {
	return Cart.find({userId: data.id}).then(result => {
		return result;
	})
}

//Create cart
module.exports.addToCart = async (data) => {
	const cartData = ({
		productId: data.reqBody.productId,
		quantity: data.reqBody.quantity,
		
	})
	console.log(cartData)

	let cart = await Cart.findOne({userId: data.user.id});
	let product = await Product.findOne({_id: cartData.productId});
	console.log(product)
	console.log(cart)

	if(!product) {
		return false;
	}	
		const productData = ({
			name: product.name,
			price: product.price
		}) 

		if(cart) {
			let productIndex = cart.products.findIndex(p => p.productId == cartData.productId);
			console.log(productIndex)
			if(productIndex > -1) {
				let productItem = cart.products[productIndex]; 
					productItem.quantity += cartData.quantity;
					cart.products[productIndex] = productItem;
			}
			else {
				cart.products.push({productId: cartData.productId, name: productData.name, quantity: cartData.quantity, price: productData.price, url: cartData.url})
			}
			cart.bill += productData.price * cartData.quantity;

			return cart.save().then((result, error) => {
				if(error) {
					return false;
				}
				else {
					return true;
				}
			})
	} else {
		cart = new Cart({
			userId: data.user.id,
			products: [{productId: data.reqBody.productId, name: productData.name, quantity: data.reqBody.quantity, price: productData.price, url: data.reqBody.url}],
			bill: productData.price * data.reqBody.quantity
		});

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})
	}
};

//Remove item from cart
module.exports.removeProduct = async (data) => {
	//const {userId, productId} = data;
	let cart = await Cart.findOne({userId: data.userId});
	let productIndex = cart.products.findIndex(p => p.productId == data.productId);

	if(productIndex > -1) {
		let productItem = cart.products[productIndex];
			cart.bill -= productItem.quantity*productItem.price;
        	cart.products.splice(productIndex,1);
	}
	return cart.update(cart).then((result, error) => {
		if(error) {
			return false;
		}
		else {
			return cart;
		}
	})
};

//delete cart
module.exports.deleteCart = (userId) => {
	return Cart.deleteOne({userId: userId}).then(result => {
		return result;
	})
};